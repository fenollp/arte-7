#Arte+7 videos backup
A UNIX Shell script that accepts command-line requests to download
`videos.arte.tv`'s videos.

##Synopsis

    :::bash
    ./arte+7.sh  ‹destinating folder› ‹Arte+7 URL› [ ‹Arte+7 URL› ]⁺

###Case Study

Here you have a show you would like to backup, pick its address. It should look
like `http://videos.arte.tv/fr/videos/debat-europe-ou-chaos--7291340.html`.

Download the show with:

    :::bash
    ./arte+7.sh ~/Downloads http://videos.arte.tv/fr/videos/debat-europe-ou-chaos--7291340.html

##Requirements
* rtmpdump (known to work with 2.3)
* wget
