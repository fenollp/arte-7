#!/bin/bash

usage() {
    echo "Usage: $0  ‹Destinating Folder› ‹Arte+7 URL› [ ‹Arte+7 URL› ]⁺"
    echo "Requires: rtmpdump, wget."
    exit 1
}
[[ $# -lt 2 ]] && usage

print () {
    #return # Sets verbose off.
    echo -ne "\033[01;34m$1"
    shift 1
    echo -e " :: \033[00m\033[01;01m$*\033[00m"
}

UA="Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_4) AppleWebKit/536.11 (KHTML, like Gecko) Chrome/20.0.1132.47 Safari/536.11"

D="${1}"

until [ "$2" = '' ]; do
    arte="${2}"

    inter=$( echo $arte | grep -Eo "[0-9]{6,}" )
    F="${D}/tmp_${inter}.htm"
    print "Destination Folder" $D

    wget --quiet -O $F --user-agent="${UA}" $arte
    first=$( cat $F | grep vars_player | grep videorefFileUrl | grep -Eo "=\s*\"[^\"]+\"" | sed 's/=//' | sed 's/"//g' )
    print first $first

    if [ ! -f $F ]; then
        echo "Pas pu accéder à la vidéo"
        echo "…ou alors l'ordre des arguments est mauvais"
        usage
    fi

    wget --quiet -O $F --user-agent="${UA}" $first
    second=$( cat $F | grep video | grep lang | grep '"fr"' | grep -Eo "ref=\"[^\"]+\"" | sed 's/ref=//' | sed 's/"//g' )
    print second $second

    wget --quiet -O $F --user-agent="${UA}" $second
    third=$( cat $F | grep rtmp | grep -Eo '"hd">[^><]+</' | sed 's/"hd">//' | sed 's/<\///' )

    url=$third
    name=$( cat $F | grep name | head -n 1 | sed 's/<name>//' | sed 's/<\/name>//' | sed 's/\//,/g' | sed 's/:/,/g' )
    print "Stream Remote Address" $url

    rm $F
    fn="${D}/${name} - Arte+7".flv
    rtmpdump -r "${url}" -o "$fn" # --quiet
    print "File Saved at" "$fn"
    echo -e "$arte\t$(date)\t$url" >> ~/.dled

    shift
done
